let form = document.getElementById('cadastro');

form.addEventListener("submit", (e) => {
    e.preventDefault();
    
    let nome = document.getElementById('nome').value;
    let email = document.getElementById('email').value;
    
    let data = {
        nome,
        email,
    };
    let convertData = JSON.stringify(data);

    localStorage.setItem('cadastro', convertData);

    let content = document.getElementById('cadastro')
   
    const arrayName = nome.split(" ");

    content.innerHTML = `<p align="center"><h2>Ola, ${arrayName[0]}! Seu cadastro foi feito com sucesso! </h2></p><br><br>`;

})